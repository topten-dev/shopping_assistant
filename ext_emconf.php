<?php

$EM_CONF[$_EXTKEY] = array(
	'title' => 'Shopping Assistant',
	'description' => 'Shopping assistant API topten.ch',
	'category' => 'plugin',
	'author' => 'Julian Kleinhans',
	'author_email' => 'julian.kleinhans@aijko.com',
	'author_company' => 'AIJKO GmbH',
	'shy' => '',
	'version' => '1.0.0',
	'priority' => '',
	'module' => '',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '0',
	'createDirs' => '',
	'modify_tables' => '',
	'clearCacheOnLoad' => 0,
	'lockType' => '',
	'constraints' => array(
		'depends' => array(
			'cms' => '',
			'extbase' => '',
			'fluid' => '',
			'core' => '',
			'aijko_default' => '',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);