<?php
namespace Aijko\ShoppingAssistant\Controller;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Julian Kleinhans <julian.kleinhans@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Shopping assistant controller
 *
 * @package shopping_assistant
 */
class ShoppingAssistantController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController {

	/**
	 * @var \Aijko\ShoppingAssistant\Domain\Service\ServiceInterface
	 */
	protected $service;

	/**
	 * Initialize topten service
	 */
	public function initializeAction() {
		$this->service = $this->objectManager->get('Aijko\\ShoppingAssistant\\Domain\\Service\\ServiceInterface');
		$this->service->setUsername($this->settings['service']['username']);
		$this->service->setPassword($this->settings['service']['password']);
	}

	/**
	 * Set filter
	 * 
	 * TODO: Please adapt this so every requests sets a different category each time
	 * Please note that each category request will return a sorted category by the default application settings on the API.
	 * 
	 * If you want to retrieve the Category Attributes, Settings, Filterable data, you should build a new request to the endpoint
	 * /api/v1/category-settings.
	 * More info: https://documenter.getpostman.com/view/552930/topten-switzerland-api/71FXWvF#1ae4fcb4-a323-6ef0-6a5f-1ab036a0e915
	 * 
	 * @return void
	 */
	public function filterAction() {
		// set category for the request
		// execute request and retrieve results in JSON format
		// send the data to the view

		//$this->service->setAction('categories')->execute(); this should be changed!
		$this->view->assign('filter', $this->service->getResultAsJson());
		$this->view->assign('settings', $this->settings);
	}

	/**
	 * Get results via ajax request
	 * 
	 * TODO: match with appropiate functions on ToptenService Class
	 *
	 * @param string $category The category code of the product list
	 * @return void
	 */
	public function resultsAction($category) {
		$results = null;
		if ($category) {
			$this->service->setPerPage($this->settings['products']['maxResults']); // You can let the API know how many products you want
			$this->service->setCategory($category); // You always need to set a Category to get the best results
			$this->service->execute();
			$results = $this->service->getResult();
		}

		$this->view->assign('resultCount', count($results));
		$this->view->assign('results', $results);
	}



}