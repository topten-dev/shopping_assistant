<?php
namespace Aijko\ShoppingAssistant\Domain\Service;

/***************************************************************
 *  Copyright notice
 *
 *  (c) 2014 Julian Kleinhans <julian.kleinhans@aijko.com>, AIJKO GmbH
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
 * Shopping assistant service interface
 *
 * @package shopping_assistant
 */
interface ServiceInterface {


	/**
	 * @param $key
	 * @return $this
	 */
	public function setKey($key);

	/**
	 * @return string
	 */
	public function getKey();

	/**
	 * @param string $action
	 * @return $this
	 */
	public function setAction($action);

	/**
	 * @return string
	 */
	public function getAction();

	/**
	 * @param int $applicationId
	 * @return $this
	 */
	public function setApplicationId($applicationId);

	/**
	 * @return int
	 */
	public function getApplicationId();

	/**
	 * @param array $parameter
	 * @return $this
	 */
	public function setAdditionalParameter(array $parameter);

	/**
	 * @return array
	 */
	public function getAdditionalParameter();

	/**
	 * @return array
	 */
	public function getResult();

	/**
	 * @return string
	 */
	public function getResultAsJson();

	/**
	 * @return void
	 * @throws \Exception
	 */
	public function execute();

	/**
	 * @param $results
	 * @param int $maxProducts
	 * @return \stdClass
	 */
	public function prepareResults($results, $maxProducts);

}