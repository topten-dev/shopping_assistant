<?php
namespace Aijko\ShoppingAssistant\Domain\Service;

	/***************************************************************
	 *  Copyright notice
	 *
	 *  (c) 2014 Julian Kleinhans <julian.kleinhans@aijko.com>, AIJKO GmbH
	 *
	 *  All rights reserved
	 *
	 *  This script is part of the TYPO3 project. The TYPO3 project is
	 *  free software; you can redistribute it and/or modify
	 *  it under the terms of the GNU General Public License as published by
	 *  the Free Software Foundation; either version 3 of the License, or
	 *  (at your option) any later version.
	 *
	 *  The GNU General Public License can be found at
	 *  http://www.gnu.org/copyleft/gpl.html.
	 *
	 *  This script is distributed in the hope that it will be useful,
	 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
	 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	 *  GNU General Public License for more details.
	 *
	 *  This copyright notice MUST APPEAR in all copies of the script!
	 ***************************************************************/

/**
 * Topten.ch webservice API
 * 
 * For more details about the API, please visit:
 * https://documenter.getpostman.com/view/552930/topten-switzerland-api/71FXWvF
 * 
 * Or get the interactive version:
 * https://www.getpostman.com/collections/2e4c298b41bf5b02e691
 * 
 * Further assistance: Victor Gonzalez <victor.gonzalez@topten.ch>
 *
 * @package shopping_assistant
 */
class ToptenService implements \Aijko\ShoppingAssistant\Domain\Service\ServiceInterface {

	const DEFAULT_LANGUAGE = 'de_CH';
	const SERVICE_URL = 'http://www.topten.ch/api/v1/product';

	/**
	 * @var string Available options: de_CH, it_CH, fr_CH. API defaults to de_CH
	 */
	protected $language = DEFAULT_LANGUAGE;

	/**
	 * @var string API username
	 */
	protected $username;

	/**
	 * @var string API password
	 */
	protected $password;

	/**
	 * @var int Sets page on Topten API product pagination
	 */
	protected $page;

	/**
	 * @var int Sets the amount of items per-page on Topten API product pagination
	 */
	protected $perPage;

	/**
	 * @var string Sets the product category to fetch
	 */
	protected $category;

	/**
	 * @var string
	 */
	protected $rawResult;

	/**
	 * @param $language
	 * @return $this
	 */
	public function setLanguage($language) {
		$this->language = $language;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getLanguage() {
		return $this->language;
	}

	/**
	 * @param $username
	 * @return $this
	 */
	public function setUsername($username) {
		$this->username = $username;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getUsername() {
		return $this->username;
	}

	/**
	 * @param $password
	 * @return $this
	 */
	public function setPassword($password) {
		$this->password = $password;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPassword() {
		return $this->password;
	}

	/**
	 * @param string $page
	 * @return $this
	 */
	public function setPage($page) {
		$this->page = $page;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPage() {
		return $this->page;
	}

	/**
	 * @param string $perPage
	 * @return $this
	 */
	public function setPerPage($perPage) {
		$this->perPage = $perPage;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getPerPage() {
		return $this->perPage;
	}

	/**
	 * @param string $category
	 * @return $this
	 */
	public function setCategory($category) {
		$this->category = $category;
		return $this;
	}

	/**
	 * @return string
	 */
	public function getCategory() {
		return $this->category;
	}

	/**
	 * @return array
	 */
	public function getResult() {
		return json_decode($this->getResultAsJson());
	}

	/**
	 * @return string
	 */
	public function getResultAsJson() {
		return $this->rawResult;
	}

	/**
	 * @return string
	 */
	public function buildQueryParams()
	{
		$data = [
			'language' => $this->getLanguage(),
			'category' => $this->getCategory(),
			'page' => $this->getPage(),
			'per-page' => $this->getPerPage(),
		];

		return http_build_query(array_filter($data));
	}

	public function buildAuthorizationToken()
	{
		return  "Authorization: Basic " . base64_encode($this->username . ':' . $this->password);
	}

	/**
	 * @return void
	 * @throws \Exception
	 */
	public function execute() {
		try {
			if (!$this->getUsername()) {
				throw new \Exception('You must set the API username! (EXT:shopping_assistant, Line: ' . __LINE__ . ', File: ' . __FILE__ . ')');
			}

			if (!$this->getPassword()) {
				throw new \Exception('You must set the API password! (EXT:shopping_assistant, Line: ' . __LINE__ . ', File: ' . __FILE__ . ')');
			}

			if (!function_exists('curl_version')) {
				throw new \Exception('You must have the cURL library enabled! (EXT:shopping_assistant, Line: ' . __LINE__ . ', File: ' . __FILE__ . ')');
			}

			// build the url
			$url = self::SERVICE_URL . '?' . $this->buildQueryParams();

			// retrieve the data
			$curl = curl_init();
			
			curl_setopt_array($curl, [
				CURLOPT_URL => $url,
				CURLOPT_RETURNTRANSFER => true,
				CURLOPT_ENCODING => "",
				CURLOPT_MAXREDIRS => 10,
				CURLOPT_TIMEOUT => 30,
				CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
				CURLOPT_CUSTOMREQUEST => "GET",
				CURLOPT_HTTPHEADER => [
					$this->buildAuthorizationToken(),
					"cache-control: no-cache",
				],
			]);
			
			$response = curl_exec($curl);
			$err = curl_error($curl);
			
			curl_close($curl);
			
			if ($err) {
				throw new \Exception('API request failed: ' . $url . '  (EXT:shopping_assistant, Line: ' . __LINE__ . ', File: ' . __FILE__ . ')');
			}

			// data comes in valid JSON format
			$this->rawResult = $response;

		} catch(\Exception $e) {
			die($e->getMessage());
		}
	}
}