# Shopping Assistant (Typo3 Extension)

## Topten API

For full details on the Topten API, please visit:

https://documenter.getpostman.com/view/552930/topten-switzerland-api/71FXWvF

For an interactive version of the API, please [download Postman](https://www.getpostman.com/apps), and open the following link:

https://www.getpostman.com/collections/2e4c298b41bf5b02e691

For further assistance, please reach [Ian Rothwell \<ian.rothwell@topten.ch\>](mailto:ian.rothwell@topten.ch) (in German and English), or the main developer of the Topten Platform [Victor Gonzalez \<victor.gonzalez@topten.ch\>](mailto:victor.gonzalez@topten.ch) (English only).

Also, please free to send inquires through our Gitlab repositories here:

https://gitlab.com/topten-dev/shopping_assistant/issues

## Important issues

1. The code presented here **is not tested, and will not work**, and it's whole purpose is to give some guidance on where to go to make this extension usable again. Considering this, the `ToptenService` class, is at least in its minimal competent format, and *should* work.

2. The most affected file is the `Classes/Domain/Servie/ToptenService.php` file, which is the internal wrapper to the new Topten API. Already is included most of the biggest changes, including the parameters and the code associated with it.

3. Also affected is the setup file on `Configuration/TypoScript/setup.txt` and the `Classes/Controller/ShoppingAsssitantController.php` with some minor changes to reflect the `ToptenService` class changes.

4. You need to update the `Classes/Domain/Servie/ServiceInterface.php` to match the signatures of the `ToptenService` class.

5. The views have not been modified! Please update them as needed.

## Further Steps

If you need any further assistance with the next steps, please drop us an email, or leave a new issue in this repository.