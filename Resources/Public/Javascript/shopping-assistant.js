(function($) {
    /**
   * ShoppingAssistant
   * @date 2014-05-23
   * @constructor
   */
    function ShoppingAssistant($root, conf) {
        var self = this;
        var fire = $root.add(self);
        var currentCategory = conf.initialCategory;
        var currentProductList = null;
        var $filter = $root.find(conf.selectorFilter);
        var $templates = $("<div/>", {
            html: $root.find(conf.selectorTemplates).html()
        });
        var $categories = $templates.find(conf.selectorCategories);
        var $categoryItem = $categories.down().clone();
        $categories.empty().appendTo($filter);
        var $refineContainer = $("<div/>", {
            "class": "sa-refine"
        }).appendTo($filter);
        var $refineSubcategory = $templates.find(conf.selectorRefineSubcategory);
        var $refineSubsubcategory = $templates.find(conf.selectorRefineSubsubcategory);
        var $productListContainer = $("<div/>", {
            "class": "sa-result"
        }).appendTo($root);
        var productListUrl = $filter.attr("action");
        /**
         * Add public API-Methods
         */
        $.extend(self, {

            /**
             * Function to get current configuraiton
             * @returns {Object} configuration
             */
            getConf: function() {
                return conf;
            },

            /**
             * Function to get categories
             * @returns {Object} categories
             */
            getCategories: function() {
                return conf.categories;
            },

            /**
             * Function to get single category
             * @param {Int} id - id of category
             * @returns {Object} category
             */
            getCategory: function(id) {
                return self.getCategories()[id];
            },

            /**
             * Function to get current category
             * @returns {Object} category
             */
            getCurrentCategory: function() {
                return self.getCategories()[currentCategory];
            },

            /**
             * Shows category by id and update subcategories
             * @param {Int} id - id of category
             */
            showCategory: function(id) {
                currentCategory = id;
                var category = self.getCategory(id);
                $refineContainer.empty();
                // if there is a productlists attribute in root level load list
                if (category.productlists && category.productlists.length) {
                    self.showProductList(category.productlists[0].id);
                } else {
                    _renderSubcategories(category.subcategories);
                }
                fire.trigger("onShowCategory", [ id, category ]);
            },

            /**
             * Shows productlist
             * @param {Int} plid - productlist id
             */
            showProductList: function(plid) {
                currentProductList = plid;
                var ajaxData = {};
                ajaxData[conf.ajaxPlidParameter] = plid;
                $productListContainer.slideUp(function() {
                    $productListContainer.empty().addClass('loading');
                    $.get(productListUrl, ajaxData, function(data, status) {
                        $productListContainer.removeClass('loading').html(data).slideDown(function() {
                            fire.trigger("onResultsLoaded", [ plid ]);
                        });
                    });
                });
            }
        });
        /**
         * Add private functions
         */

        /**
         * Add subcategory select box to dom and add eventhandler
         * @param {Object} subcategories - subcategory object extracted from category
         */
        function _renderSubcategories(subcategories) {
            var $subcategories = $refineSubcategory.clone().appendTo($refineContainer).find("select");
            var $subcategoryItem = $subcategories.down().detach();
            $.each(subcategories, function(id, subcategory) {
                var $subcategory = $subcategoryItem.clone();
                $subcategory.val(id).text(subcategory.name);
                $subcategory.appendTo($subcategories);
            });
            $subcategories.on("change.ShoppingAssistant", function() {
                var subcategory = self.getCurrentCategory().subcategories[$subcategories.val()];
                var subcategoryProductLists = subcategory.productlists;
                $refineContainer.find(conf.selectorRefineSubsubcategory).remove();
                if (subcategoryProductLists.length == 1) {
                    self.showProductList(subcategoryProductLists[0].id);
                } else {
                    _renderSubsubcategories(subcategoryProductLists);
                }
            }).trigger("change.ShoppingAssistant");
        }

        /**
         * Add subsubcategories select box to dom and add eventhandler
         * @param {Object} subsubcategories - subsubcategories object extracted from category's subcategory
         */
        function _renderSubsubcategories(subsubcategories) {
            var $subsubcategories = $refineSubsubcategory.clone().appendTo($refineContainer).find("select");
            var $subsubcategoryItem = $subsubcategories.down().detach();
            $.each(subsubcategories, function(id, subsubcategory) {
                var $subsubcategory = $subsubcategoryItem.clone();
                $subsubcategory.val(subsubcategory.id).text(subsubcategory.name);
                $subsubcategory.appendTo($subsubcategories);
            });
            $subsubcategories.on("change.ShoppingAssistant", function() {
                self.showProductList($subsubcategories.val());
            }).trigger("change.ShoppingAssistant");
        }

        /**
         * Add callbacks
         */
        $.each([ "onInit", "onShowCategory", "onResultsLoaded" ], function(i, name) {
            // via configuration
            if ($.isFunction(conf[name])) {
                $(self).on(name, conf[name]);
            }
            // via API
            self[name] = function(fn) {
                if (fn) {
                    $(self).on(name, fn);
                }
                return self;
            };
        });
        /**
         * Initialization
         */
        $.each(self.getCategories(), function(id, category) {
            var $category = $categoryItem.clone();
            $category.find("span").text(category.name);
            // Set initial category input to checked
            if (id == conf.initialCategory) {
                $category.find("input").attr("checked", "checked");
            }
            $category.on("change.ShoppingAssistant", function() {
                self.showCategory(id);
            });
            $category.appendTo($categories);
        });
        self.showCategory(conf.initialCategory);
        fire.trigger("onInit");
    }
    // ShoppingAssistant end
    /**
     * jQuery plugin implementation
     * @param {Object} conf - Plugin configuration
     * @return {DOM|Object} - DOM or ShoppingAssistant-API
     */
    $.fn.ShoppingAssistant = function(conf) {
        var el = this.data("ShoppingAssistant");
        if (el) {
            return el;
        }
        var defaults = {
            selectorFilter: "#sa-filter",
            selectorTemplates: "#sa-templates",
            selectorCategories: "#sa-categories",
            selectorRefineSubcategory: "#sa-refine-subcategory",
            selectorRefineSubsubcategory: "#sa-refine-subsubcategory",
            ajaxPlidParameter: "tx_shoppingassistant_shoppingassistant[plid]",
            initialCategory: 0
        };
        var conf = $.extend({}, defaults, conf);
        this.each(function() {
            el = new ShoppingAssistant($(this), conf);
            $(this).data("ShoppingAssistant", el);
        });
        return conf.api ? el : this;
    };
    /**
     * Trigger Event that this file is loaded so
     * the plugin can 
     */
    $(function() {
        $(window).trigger("ShoppingAssistantLoaded");
    });
})(jQuery);